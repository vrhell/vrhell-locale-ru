<?php

 $LocaleInfo['vf_ru'] = array (
  'Locale' => 'ru',
  'Name' => 'Русский / Russian',
  'Description' => 'Official Russian language translations for Vanilla. Help contribute to this translation by going to its translation site <a href="https://www.transifex.com/projects/p/vanilla/language/ru/">here</a>.',
  'Version' => '2016.02.18p1401',
  'Author' => 'Vanilla Community',
  'AuthorUrl' => 'https://www.transifex.com/projects/p/vanilla/language/ru/',
  'License' => 'CC BY-SA 4.0',
);
